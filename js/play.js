// https://stackoverflow.com/questions/1007981/how-to-get-function-parameter-names-values-dynamically
var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var ARGUMENT_NAMES = /([^\s,]+)/g;
var gInit = false;

function getParamNames(func) {
    var fnStr = func.toString().replace(STRIP_COMMENTS, '');
    var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
    if (result === null)
        result = [];
    return result;
}




var playState = {

    preload: function () {

        game.load.image("player", "assets/ball.png");
        game.load.image("bigplanet", "assets/pixel.png");
        game.load.image("bag", "assets/item.png");

        game.load.image("goal", "assets/goal.png");
        game.load.image("wall", "assets/wall.png");
        game.load.image("bwall", "assets/bwall.png");

        game.load.image("playButton", "assets/play.png");
        game.load.image("pauseButton", "assets/pause.png");
        game.load.image("menuButton", "assets/menu.png");

        /*-------------------------------新增--------------------------------------------------- */
        game.load.spritesheet('bullet', 'assets/rgblaser.png', 4, 4);
        /*-------------------------------------------------------------------------------------- */

        /*----------------------------------新增音效-----------------------------------*/
        game.load.audio('destroy', 'music/destroy.mp3');
        game.load.audio('launch', 'music/launch.mp3');
        game.load.audio('shoot', 'music/shoot.mp3');
        game.load.audio('attraction', 'music/attraction.mp3');
        /*----------------------------------------------------------------------------*/

    },

    create: function () {

        // 之所以設定 priority 是為了這個問題：
        // https://stackoverflow.com/questions/27650964/phaser-js-how-to-stop-event-propagationfiring-from-textbutton-events-oninputdo        

        this.gameMode = 0;

        // 發射的時候得到位置用的
        this.touchLayer = game.add.sprite(0, 0);
        this.touchLayer.width = 800;
        this.touchLayer.height = 600;
        this.touchLayer.inputEnabled = true;
        this.touchLayer.input.priorityID = 0; // lower priority
        this.touchLayer.events.onInputDown.add((obj, pt) => {
            if (!this.player.Started && this.gameMode) {
                game.physics.arcade.moveToPointer(this.player, 500, game.input.activePointer);
                this.player.Started = true;

                /*----------------------------------新增音效-----------------------------------*/
                this.MusicLaunch.play();
                /*----------------------------------------------------------------------------*/
            }
        });

        // 物品欄Sprite
        this.bag = game.add.sprite(280, 539, 'bag');
        game.physics.arcade.enable(this.bag);

        // 發射物Sprite
        this.player = game.add.sprite(0, 600, 'player');
        this.player.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.player);
        this.player.body.bounce.x = 1;
        this.player.body.bounce.y = 1;
        this.player.body.collideWorldBounds = true;

        /*-------------------------------新增--------------------------------------------------- */
        //武器
        this.weapon = game.add.weapon(40, 'bullet');
        this.weapon = this.weapon.setBulletFrames(0, 80, true);
        this.weapon.enableBody = true;
        this.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.weapon.bulletSpeed = 1000;
        this.weapon.fireRate = 100;
        this.weapon.trackSprite(this.player, 0, 0, true);
        /*-------------------------------------------------------------------------------------- */

        // 用來畫星球的引力範圍
        this.painterGroup = game.add.group();

        // 包含終點的group
        this.goalGroup = game.add.group();
        this.goalGroup.enableBody = true;

        // 包含障礙物的group
        this.wallGroup = game.add.group();
        this.wallGroup.enableBody = true;

        // 包含星球的group
        this.planetGroup = game.add.group();
        this.planetGroup.enableBody = true;

        // 遊戲重來按鈕
        this.button = game.add.sprite(800 - 20 - 160 * 0.4, 600 - 20 - 160 * 0.4, 'playButton');
        this.button.scale.setTo(0.4, 0.4);
        this.button.inputEnabled = true;
        this.button.input.priorityID = 1;  // higher priority
        this.button.events.onInputDown.add((obj, pt) => {
            if (this.gameMode) {
                this.playerDie();
            } else {
                this.switchMode("Playing");
            }
        });

        this.menu = game.add.sprite(800 - 2 * 20 - 2 * 160 * 0.4, 600 - 20 - 160 * 0.4, 'menuButton');
        this.menu.scale.setTo(0.4, 0.4);
        this.menu.inputEnabled = true;
        this.menu.input.priorityID = 1;  // higher priority
        this.menu.events.onInputDown.add((obj, pt) => {
            //--- 挑戰模式或普通模式案下的動作 ---//
            if (game.global.challenge == 1) {
                game.global.challenge = 0;
                game.global.level = 1;
                game.state.start("stageState");
            }
            else
                game.state.start("stageState");
        });

        this.initEditor();
        this.time = 0;


        // 死亡次數文字的顯示
        this.diedCount = game.add.text(570, 20, 'Died:', { font: '30px Arial', fill: '#ffffff' });

        //-------- 挑戰模式 --------//
        if (game.global.challenge == 0) {
            this.importLevel(game.global.content);
        } else {
            this.importLevel(game.global.content[game.global.level]);
        }

        /*----------------------------------新增音效-----------------------------------*/
        //音效
        this.MusicDestroy = game.add.audio('destroy');
        this.MusicLaunch = game.add.audio('launch');
        this.MusicShoot = game.add.audio('shoot');
        this.MusicAttraction = game.add.audio('attraction');
        /*----------------------------------------------------------------------------*/

        //this.StageChanger(game.global.level);
        this.switchMode("Layout");
    },

    shutdown: function () {
        $("#editor > div:not(.fixedFunction)").remove();
        if ($(".fixedFunction")[0].children.length >= 3) {
            $(".fixedFunction")[0].children[2].remove();
            $(".fixedFunction")[0].children[1].remove();
        }
    },

    update: function () {

        this.time++;
        this.diedCount.text = "Retry Count : " + game.global.died;

        // 在遊玩模式時
        if (this.gameMode == 1) {

            /*-------------------------------新增--------------------------------------------------- */
            if (game.input.mousePointer.middleButton.isDown) {
                let x = this.player.body.velocity.x + this.player.body.position.x;
                let y = this.player.body.velocity.y + this.player.body.position.y;
                this.weapon.fire(null, x, y);
                /*----------------------------------新增音效-----------------------------------*/
                this.MusicShoot.play();
                /*----------------------------------------------------------------------------*/

            }
            /*-------------------------------------------------------------------------------------- */

            this.button.loadTexture('pauseButton');
            this.planetGroup.setAll("inputEnabled", false);

            //game.physics.arcade.overlap(this.wallGroup, this.player, this.playerDie, null, this);
            this.wallGroup.forEach((tile) => {
                game.physics.arcade.overlap(tile, this.player, (tile) => {
                    if (tile.visible) {
                        this.playerDie();
                    }
                }, null, this);
            });

            game.physics.arcade.overlap(this.goalGroup, this.player, () => {
                game.global.level += 1;
                //game.state.start("breakState");
                //game.state.start("stageState");

                /////////
                game.state.start("breakState");
                /////////

                this.switchMode("Layout");
            }, null, this);

            // 在布局模式時
        } else if (this.gameMode == 0) {

            this.button.loadTexture('playButton');
            this.planetGroup.forEach((v, i) => {
                if (v.MoveTag) {
                    v.inputEnabled = true;
                }
            })
        }

        this.wallGroup.forEach(function (tile) {
            playState.weapon.bullets.forEach(function (bullets) {
                game.physics.arcade.overlap(tile, bullets, () => {
                    if (tile.Breakable) {
                        tile.visible = false;
                    }

                    if (tile.visible) {
                        bullets.kill();
                    }
                }, null, this);
            });
        });

        this.planetGroup.forEach((v, i) => {
            this.planetUpdate(v);
        });
    },





    // add 系列函數的神祕：
    // 1. 關卡的 structure 的 member 名稱與函數的引數要一致
    // 2. 第一個參數必須是 exist，代表之前有沒有此物件，若為 null 代表要創一個新的；若不為 null 代表更新其狀態
    // 3. 如果 exist 是 null，傳回值為新創的物件；否則傳回 exist

    // 真正的新增終點
    addGoal(exist, posX, posY, width, height) {
        if (!exist) {
            var goal = game.add.sprite(posX, posY, "goal");
            this.goalGroup.add(goal);
        } else {
            var goal = exist;
            exist.x = posX;
            exist.y = posY;
        }

        goal.scale.setTo(width / 30, height / 30);
        goal.body.immovable = true;

        return goal;
    },

    // 真正的新增障礙物
    addWall: function (exist, posX, posY, width, height) {
        if (!exist) {
            var wall = game.add.sprite(posX, posY, "wall");
            this.wallGroup.add(wall);
        } else {
            var wall = exist;
            wall.x = posX;
            wall.y = posY;
        }

        wall.scale.setTo(width / 30, height / 30);
        wall.body.immovable = true;

        return wall;
    },

    // 真正的新增障礙物
    addBreakableWall: function (exist, posX, posY, width, height) {
        if (!exist) {
            var wall = game.add.tileSprite(posX, posY, width, height, "bwall");
            wall.physicsType = Phaser.SPRITE;
            this.wallGroup.add(wall);
        } else {
            var wall = exist;
            wall.x = posX;
            wall.y = posY;
            wall.width = width;
            wall.height = height;
            wall.body.setSize(width, height);
        }

        wall.Breakable = true;        
        wall.body.immovable = true;

        return wall;
    },

    // 真正的新增星球
    addPlanet: function (exist, posX, posY, gravityRadius, type, param) {
        if (!exist) {
            var planet = game.add.sprite(posX, posY, "bigplanet");
            this.planetGroup.add(planet);

            planet.Painter = game.add.graphics();
            this.painterGroup.add(planet.Painter);
            planet.Painter.anchor.setTo(0.5, 0.5);

        } else {
            var planet = exist;
            planet.x = posX;
            planet.y = posY;
        }

        planet.GravityRadius = gravityRadius;
        planet.Time = Math.random() * 100;
        planet.anchor.setTo(0.5, 0.5);




        // 請當成 self-documentation 來理解 type 有哪些與 param 有哪些QQ
        switch (type) {

            case "Static":
                planet.body.static = true;
                break;

            case "Item":
                // 如果有 MoveTag 這個變數，就代表在布局模式時可以移動
                planet.MoveTag = true;
                planet.inputEnabled = true;
                planet.input.enableDrag(false, true);
                break;

            case "SHM":
                /**
                 * param {
                 *      XR : X 方向的位移
                 *      XT : X 方向的週期(sec)
                 *      XPhi : X方向的偏移週期(幾個 XT)
                 *      YR : Y 方向的位移
                 *      YT : Y 方向的週期(sec)
                 *      YPhi : Y 方向的偏移週期(幾個 YT)
                 * }
                 */

                param.XR = param.XR || 0;
                param.XT = param.XT || 1;
                param.XPhi = param.XPhi || 0;

                param.YR = param.YR || 0;
                param.YT = param.YT || 1;
                param.YPhi = param.YPhi || 0;


                planet.UpdateFunc = () => {
                    var time = this.time;

                    var xf = time / 60 / param.XT + param.XPhi * param.XT;
                    var xr = xf * 2 * Math.PI;
                    planet.x = posX + Math.sin(xr) * param.XR;

                    var yf = time / 60 / param.YT + param.YPhi * param.YT;
                    var yr = yf * 2 * Math.PI;
                    planet.y = posY + Math.sin(yr) * param.YR;
                };
                break;
        }

        return planet;

    },








    planetUpdate: function (planetG) {

        var player1 = this.player;
        planetG.Time++;


        // 檢查星球是否在物品欄中 (是: 沒引力, 否:有引力)
        if (!game.physics.arcade.overlap(planetG, this.bag)) {

            planetG.Painter.clear();
            planetG.Painter.lineStyle(2, 0xffffff, 0.2);
            planetG.Painter.beginFill(0x000000, 0.2);
            planetG.Painter.drawCircle(planetG.x, planetG.y, planetG.GravityRadius * 2 + Math.sin(planetG.Time / 120 * Math.PI * 2) * 10);
            planetG.Painter.endFill();


            if (planetG.UpdateFunc) {
                planetG.UpdateFunc();
            }

            if (!this.gameMode) {
                return;
            }


            // 檢查玩家發射物是否進入到星球引力範圍
            var distance = Phaser.Math.distance(player1.x, player1.y, planetG.x, planetG.y);

            // 如果進入到星球引力範圍
            if (distance < planetG.width / 2 + planetG.GravityRadius) {

                // 運算進入星球引力範圍的發射物與該星球的夾角
                var angle = Phaser.Math.angleBetween(player1.x, player1.y, planetG.x, planetG.y);

                // 發射物受到引力影響的方向改變 (調整factor值來控制引力影響效果)
                var factor = 2;
                var force = (planetG.GravityRadius - distance); // 用發射物與星球的距離來決定引力影響大小
                this.player.body.velocity.x += Math.cos(angle) * force * factor;
                this.player.body.velocity.y += Math.sin(angle) * force * factor;

                // 調整發射物的最高速度 (因星球的引力有時會讓發射物加速到爆炸) 暴力法，待解決
                if (this.player.body.velocity.x > 500)
                    this.player.body.velocity.x = 500;
                if (this.player.body.velocity.y > 500)
                    this.player.body.velocity.y = 500;

                // 調整發射物的最高速度 (因星球的引力有時會讓發射物慢到爆炸) (待解決)
                //
                //
                //

                /*----------------------------------新增音效-----------------------------------*/
                this.MusicAttraction.play();
                /*----------------------------------------------------------------------------*/
            }

        } else {
            planetG.Painter.clear();
        }

    },

    // "Playing", "Layout"
    switchMode: function (mode) {
        switch (mode) {

            case "Playing":
                this.gameMode = 1;
                break;

            case "Layout":
                playState.weapon.bullets.forEach(function (bullets) {
                    bullets.kill();
                });

                this.wallGroup.forEach(function (tile) {
                    tile.visible = true;
                });

                // 發射物回到原點
                this.player.x = this.level.playerInitialPosition.x;
                this.player.y = this.level.playerInitialPosition.y;
                // 發射物速度歸0
                this.player.body.velocity.x = 0;
                this.player.body.velocity.y = 0;
                // 回到布置模式
                this.gameMode = 0;
                this.player.Started = null;
                break;

        }
    },

    // 發射物接觸到障礙物的函式
    playerDie: function () {
        /*----------------------------------新增音效-----------------------------------*/
        this.MusicDestroy.play();
        /*----------------------------------------------------------------------------*/

        game.global.died += 1;
        this.switchMode("Layout");
    },








    importLevel: function (levelString) {

        this.goalGroup.removeAll();
        this.wallGroup.removeAll();
        this.painterGroup.removeAll();
        this.planetGroup.removeAll();

        this.level = JSON.parse(levelString);

        $("#editor > div:not(.fixedFunction)").remove();

        // TODO: very ugly......
        // player initial position
        if ($(".fixedFunction")[0].children.length >= 3) {
            $(".fixedFunction")[0].children[2].remove();
            $(".fixedFunction")[0].children[1].remove();
        }
        this.genField($(".fixedFunction")[0], this.level.playerInitialPosition, () => {
            this.player.x = this.level.playerInitialPosition.x;
            this.player.y = this.level.playerInitialPosition.y;
        });
        this.switchMode("Layout");

        this.level.goal.forEach((v, i) => {
            let item = document.createElement("div");
            $("#editor")[0].appendChild(item);
            item.innerText = "Goal";

            var func = this.addGoal;
            var param = v;

            var ret = this.addWrapper(func, param);
            param.exist = ret;
            this.genField(item, param, () => {
                this.addWrapper(func, param);
            });
        });

        this.level.wall.forEach((v, i) => {
            let item = document.createElement("div");
            $("#editor")[0].appendChild(item);

            if (v.breakable) {
                var func = this.addBreakableWall;
                var param = v;
                item.innerText = "Breakable Wall";
            } else {
                var func = this.addWall;
                var param = v;
                item.innerText = "Wall";
            }

            var ret = this.addWrapper(func, param);
            param.exist = ret;
            this.genField(item, param, () => {
                this.addWrapper(func, param);
            });
        });

        this.level.planet.forEach((v, i) => {
            let item = document.createElement("div");
            $("#editor")[0].appendChild(item);
            item.innerText = v.type + " Planet";

            var func = this.addPlanet;
            var param = v;

            var ret = this.addWrapper(func, param);
            param.exist = ret;
            this.genField(item, param, () => {
                this.addWrapper(func, param);
            });
        });

    },

    exportLevel: function () {
        var str = JSON.stringify(playState.level, (key, value) => {
            if (key == "exist") {
                return undefined;
            }
            return value;
        });

        return str;
    },

    addWrapper: function (func, param) {
        var arr = getParamNames(func);
        var arg = []

        arr.forEach((v, i) => {
            arg.push(param[v]);
        });

        return func.apply(this, arg);
    },

    genField(parent, obj, updateFunc) {
        for (let key in obj) {
            if (key == "exist" || (typeof obj[key] != "object" && typeof obj[key] != "number"))
                continue;

            let div = document.createElement("div");
            parent.appendChild(div);

            if (typeof obj[key] === "object") {
                $(div).addClass("list");
                this.genField(div, obj[key], () => { });
            } else {
                $(div).addClass("item");
                let text = document.createElement("div");
                div.appendChild(text);
                text.innerText = key;

                let input = document.createElement("input");
                div.appendChild(input);
                input.value = obj[key];
                input.oninput = (() => {
                    obj[key] = Number(input.value);
                    updateFunc();
                });
            }
        }
    },

    initEditor: function () {

        this.goalGroup.removeAll();
        this.wallGroup.removeAll();
        this.painterGroup.removeAll();
        this.planetGroup.removeAll();

        $("#editor > div:not(.fixedFunction)").remove();

        // TODO: very ugly......
        // player initial position
        if ($(".fixedFunction")[0].children.length >= 3) {
            $(".fixedFunction")[0].children[2].remove();
            $(".fixedFunction")[0].children[1].remove();
        }



        this.level = {
            goal: [],
            wall: [],
            planet: [],
            playerInitialPosition: { x: 0, y: 590 }
        };

        this.genField($(".fixedFunction")[0], this.level.playerInitialPosition, () => {
            this.player.x = this.level.playerInitialPosition.x;
            this.player.y = this.level.playerInitialPosition.y;
        });

        if (gInit) {
            return;
        }

        gInit = true;

        var object_type = [
            "Goal",
            "Wall",
            "Static Planet",
            "SHM Planet",
            "Breakable Wall"
        ];

        object_type.forEach((v, i) => {
            let o = document.createElement("option");
            o.text = v;
            $("#editor select")[0].add(o);
        });

        $("#editorAdd").click(() => {
            let item = document.createElement("div");
            $("#editor")[0].appendChild(item);
            item.innerText = $("#editor select")[0].value;


            switch ($("#editor select")[0].value) {

                case "Goal":
                    var func = this.addGoal;
                    var param = {
                        exist: null,
                        posX: 0,
                        posY: 0,
                        width: 160,
                        height: 6
                    }
                    this.level.goal.push(param);
                    break;

                case "Wall":
                    var func = this.addWall;
                    var param = {
                        exist: null,
                        posX: 0,
                        posY: 0,
                        width: 30,
                        height: 30
                    }
                    this.level.wall.push(param);
                    break;

                case "Breakable Wall":
                    var func = this.addBreakableWall;
                    var param = {
                        exist: null,
                        posX: 0,
                        posY: 0,
                        width: 30,
                        height: 30,
                        breakable: true
                    }
                    this.level.wall.push(param);
                    break;

                case "Item Planet":
                    var planetType = "Item";
                case "Static Planet":
                    var planetType = planetType || "Static";
                case "SHM Planet":
                    var planetType = planetType || "SHM";

                    var func = this.addPlanet;
                    var param = {
                        exist: null,
                        posX: 0,
                        posY: 0,
                        gravityRadius: 100,
                        type: planetType,
                        param: {
                        }
                    }
                    this.level.planet.push(param);
                    break;
            }

            var ret = this.addWrapper(func, param);
            param.exist = ret;
            this.genField(item, param, () => {
                this.addWrapper(func, param);
            });
        });

        $("#editorUpload").click(() => {
            if (this.level.goal.length < 1) {
                alert("You haven't set any goal !");
                return;
            }

            var author = window.prompt("Your Name?") || "Anonymous";
            var name = window.prompt("Level Title") || "Pasta";
            var description = window.prompt("Level Description") || "EZ~";

            firebase.database().ref("level").push().set({
                author: author,
                name: name,
                description: description,
                timestamp: getTimeString(),
                content: this.exportLevel()
            });

        });

    },




    //****************************** 以下為內建關卡 ******************************//
    StageChanger: function () {

        var pasta = function (i, str) {
            firebase.database().ref("level").push().set({
                author: "Official",
                name: "Level " + i,
                description: "Congratulations !",
                timestamp: getTimeString(),
                content: str
            });
        };

        pasta(1, '{"goal":[{"posX":0,"posY":20,"width":6,"height":160}],"wall":[{"posX":0,"posY":180,"width":350,"height":20},{"posX":790,"posY":170,"width":10,"height":150}],"planet":[{"posX":313,"posY":565,"gravityRadius":100,"type":"Item","param":{}},{"posX":370,"posY":565,"gravityRadius":100,"type":"Item","param":{}}],"playerInitialPosition":{"x":0,"y":600}}');
        pasta(2, '{"goal":[{"posX":790,"posY":20,"width":10,"height":160}],"wall":[{"posX":600,"posY":200,"width":200,"height":30},{"posX":0,"posY":300,"width":500,"height":30}],"planet":[{"posX":313,"posY":565,"gravityRadius":100,"type":"Item","param":{}},{"posX":370,"posY":565,"gravityRadius":100,"type":"Item","param":{}}],"playerInitialPosition":{"x":0,"y":590}}');
        pasta(3, '{"goal":[{"posX":320,"posY":0,"width":160,"height":10}],"wall":[{"posX":0,"posY":150,"width":30,"height":200},{"posX":320,"posY":150,"width":30,"height":200},{"posX":450,"posY":150,"width":30,"height":200},{"posX":770,"posY":150,"width":30,"height":200}],"planet":[{"posX":440,"posY":160,"gravityRadius":100,"type":"Static","param":{}},{"posX":360,"posY":220,"gravityRadius":100,"type":"Static","param":{}},{"posX":440,"posY":280,"gravityRadius":100,"type":"Static","param":{}},{"posX":360,"posY":340,"gravityRadius":100,"type":"Static","param":{}},{"posX":313,"posY":565,"gravityRadius":100,"type":"Item","param":{}}],"playerInitialPosition":{"x":400,"y":525}}');
        pasta(4, '{"goal":[{"posX":640,"posY":0,"width":160,"height":10},{"posX":790,"posY":0,"width":10,"height":160}],"wall":[{"posX":350,"posY":0,"width":100,"height":20},{"posX":0,"posY":250,"width":20,"height":100},{"posX":780,"posY":250,"width":20,"height":100}],"planet":[{"posX":400,"posY":300,"gravityRadius":100,"type":"Static","param":{}},{"posX":300,"posY":200,"gravityRadius":100,"type":"Static","param":{}},{"posX":300,"posY":400,"gravityRadius":100,"type":"Static","param":{}},{"posX":500,"posY":200,"gravityRadius":100,"type":"Static","param":{}},{"posX":500,"posY":400,"gravityRadius":100,"type":"Static","param":{}}],"playerInitialPosition":{"x":0,"y":590}}');
        pasta(5, '{"goal":[{"posX":0,"posY":20,"width":10,"height":160}],"wall":[{"posX":0,"posY":200,"width":360,"height":20},{"posX":0,"posY":240,"width":360,"height":20},{"posX":790,"posY":150,"width":10,"height":200}],"planet":[{"posX":200,"posY":230,"gravityRadius":100,"type":"SHM","param":{"XR":180,"XT":6,"XPhi":60,"YR":0,"YT":1,"YPhi":0}},{"posX":313,"posY":565,"gravityRadius":100,"type":"Item","param":{}}],"playerInitialPosition":{"x":0,"y":590}}');
        pasta(6, '{"goal":[{"posX":320,"posY":0,"width":160,"height":6}],"wall":[{"posX":320,"posY":0,"width":30,"height":30},{"posX":450,"posY":0,"width":30,"height":30}],"planet":[{"posX":400,"posY":100,"gravityRadius":100,"type":"SHM","param":{"XR":180,"XT":5,"XPhi":0,"YR":0,"YT":1,"YPhi":0}},{"posX":400,"posY":160,"gravityRadius":100,"type":"SHM","param":{"XR":125,"XT":5,"XPhi":0,"YR":0,"YT":1,"YPhi":0}},{"posX":400,"posY":220,"gravityRadius":100,"type":"SHM","param":{"XR":65,"XT":5,"XPhi":0,"YR":0,"YT":1,"YPhi":0}},{"posX":400,"posY":280,"gravityRadius":100,"type":"SHM","param":{"XR":65,"XT":5,"XPhi":0.5,"YR":0,"YT":1,"YPhi":0}},{"posX":400,"posY":340,"gravityRadius":100,"type":"SHM","param":{"XR":125,"XT":5,"XPhi":0.5,"YR":0,"YT":1,"YPhi":0}},{"posX":400,"posY":400,"gravityRadius":100,"type":"SHM","param":{"XR":180,"XT":5,"XPhi":0.5,"YR":0,"YT":1,"YPhi":0}}],"playerInitialPosition":{"x":400,"y":525}}');
        pasta(7, '{"goal":[{"posX":350,"posY":0,"width":100,"height":10}],"wall":[{"posX":200,"posY":100,"width":400,"height":350}],"planet":[{"posX":313,"posY":565,"gravityRadius":100,"type":"Item","param":{}},{"posX":370,"posY":565,"gravityRadius":100,"type":"Item","param":{}},{"posX":428,"posY":565,"gravityRadius":100,"type":"Item","param":{}}],"playerInitialPosition":{"x":400,"y":525}}');
        pasta(8, '{"goal":[{"posX":380,"posY":260,"width":40,"height":40}],"wall":[{"posX":250,"posY":130,"width":50,"height":50},{"posX":500,"posY":130,"width":50,"height":50},{"posX":250,"posY":380,"width":50,"height":50},{"posX":500,"posY":380,"width":50,"height":50}],"planet":[{"posX":275,"posY":155,"gravityRadius":150,"type":"Static","param":{}},{"posX":275,"posY":405,"gravityRadius":150,"type":"Static","param":{}},{"posX":525,"posY":155,"gravityRadius":150,"type":"Static","param":{}},{"posX":525,"posY":405,"gravityRadius":150,"type":"Static","param":{}},{"posX":313,"posY":565,"gravityRadius":100,"type":"Item","param":{}}],"playerInitialPosition":{"x":0,"y":300}}');
        pasta(9, '{"goal":[{"posX":640,"posY":590,"width":160,"height":10}],"wall":[{"posX":200,"posY":100,"width":20,"height":500},{"posX":400,"posY":0,"width":20,"height":500},{"posX":600,"posY":200,"width":20,"height":400},{"posX":500,"posY":0,"width":200,"height":10}],"planet":[{"posX":313,"posY":565,"gravityRadius":100,"type":"Item","param":{}}],"playerInitialPosition":{"x":0,"y":590}}');

    }

}

