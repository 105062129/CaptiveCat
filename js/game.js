var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');

// globle variable 用來儲存玩家破到第幾關
game.global = {
    level: 1,
    died: 0,
    challenge: 0,
}

var bootState = {
    preload: function () {
        game.load.image("loadbar", "assets/loadbar.png");
    },

    create: function () {
        game.state.start("load");
    }
};

var loadState = {
    preload: function () {
        var bar = game.add.sprite(200, 300, "loadbar");
        bar.anchor.setTo(0, 0.5);
        game.load.setPreloadSprite(bar);


        game.load.image("level", "assets/level.png");
        game.load.image("toolButton", "assets/tool.png");
        game.load.image("chanllengeButton", "assets/challenge.png");
        game.load.image("button3", "assets/menu/button3.png");
        game.load.image("rankTitle", "assets/menu/rankTitle.png");
        game.load.image("background", "assets/menu/bg.jpg");
        game.load.image("button3", "assets/menu/button3.png");
        game.load.image("player", "assets/ball.png");
        game.load.image("bigplanet", "assets/pixel.png");
        game.load.image("bag", "assets/item.png");
        game.load.image("goal", "assets/goal.png");
        game.load.image("wall", "assets/wall.png");
        game.load.image("bwall", "assets/bwall.png");
        game.load.image("playButton", "assets/play.png");
        game.load.image("pauseButton", "assets/pause.png");
        game.load.image("menuButton", "assets/menu.png");
        game.load.spritesheet('bullet', 'assets/rgblaser.png', 4, 4);
        game.load.audio('destroy', 'music/destroy.mp3');
        game.load.audio('launch', 'music/launch.mp3');
        game.load.audio('shoot', 'music/shoot.mp3');
        game.load.audio('attraction', 'music/attraction.mp3');
        game.load.image("title", "assets/menu/title.png");
        game.load.image("star", "assets/menu/star.png");
        game.load.image("cat", "assets/menu/cat.png");
        game.load.image("bush", "assets/menu/bush.png");
        game.load.image("background", "assets/menu/bg.jpg");
        game.load.image("button1", "assets/menu/button1.png");
        game.load.image("button2", "assets/menu/button2.png");
        game.load.audio("background", "music/background4.mp3");
        game.load.image("button1", "assets/menu/button1.png");
        game.load.image("button3", "assets/menu/button3.png");
    },

    create: function () {
        game.state.start("menuState");
    }
};




game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('rankState', rankState);
game.state.add('menuState', menuState);
game.state.add('breakState', breakState);
game.state.add('playState', playState);
game.state.add('stageState', stageState);

game.state.start('boot');
