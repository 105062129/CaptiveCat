var gBGM = false;

var menuState = {

    preload: function () {

        game.load.image("title", "assets/menu/title.png");
        game.load.image("star", "assets/menu/star.png");
        game.load.image("cat", "assets/menu/cat.png");
        game.load.image("bush", "assets/menu/bush.png");
        game.load.image("background", "assets/menu/bg.jpg");
        game.load.image("button1", "assets/menu/button1.png");
        game.load.image("button2", "assets/menu/button2.png");

        /*----------------------------------新增音效-----------------------------------*/
        game.load.audio("background", "music/background4.mp3");
        /*----------------------------------------------------------------------------*/

    },

    create: function () {


        // 遊戲背景sprite
        var background = game.add.sprite(0, 0, 'background');
        background.anchor.setTo(0, 0);
        game.add.tween(background.scale).to({ x: 1.09, y: 1.09 }, 5000).yoyo(true).loop().start();

        // 顯示遊戲標題標題
        var gameName = game.add.sprite(game.width / 2, 100, 'title');
        gameName.scale.setTo(1, 1.6);
        gameName.anchor.setTo(0.5, 0.5);

        // cat屁股後的線sprite
        var star = game.add.sprite(200, 290, 'star');
        star.scale.setTo(0.7, 1);
        star.anchor.setTo(0, 0.5);
        game.add.tween(star).to({ angle: 15 }, 750).yoyo(true).loop().start();

        // cat sprite
        var cat = game.add.sprite(game.width / 2 + 150, 280, 'cat');
        cat.scale.setTo(0.3, 0.3);
        cat.anchor.setTo(0.5, 0.5);
        game.add.tween(cat).to({ y: 350 }, 750).yoyo(true).loop().start();

        // 草叢sprite
        var bush = game.add.sprite(game.width / 2 + 190, game.height / 2 + 140, 'bush');
        bush.scale.setTo(0.4, 0.4);
        bush.anchor.setTo(0.5, 0.5);
        game.add.tween(bush.scale).to({ x: 0.25, y: 0.25 }, 750).yoyo(true).loop().start();

        // 開始按鈕
        this.button1 = game.add.sprite(160, 500, 'button1')
        this.button1.scale.setTo(0.5, 0.5);
        this.button1.anchor.setTo(0.5, 0.5);
        game.add.tween(this.button1).to({ angle: -8 }, 1000).to({ angle: 8 }, 1000).to({ angle: 0 }, 1000).loop().start();
        // 點擊開始按鈕的動作
        this.button1.inputEnabled = true;
        this.button1.events.onInputDown.add((obj, pt) => {
            game.state.start('stageState');
        });

        /*----------------------------------新增音效-----------------------------------*/
        if (!gBGM) {
            this.background = game.add.audio("background");
            this.background.loop = true;
            this.background.volume = 0.5;
            this.background.play();
            gBGM = true;
        }
        /*----------------------------------------------------------------------------*/

        // 觀看排名按鈕
        this.button2 = game.add.sprite(320, 500, 'button2')
        this.button2.scale.setTo(0.4, 0.4);
        this.button2.anchor.setTo(0.5, 0.5);
        game.add.tween(this.button2).to({ angle: -8 }, 1000).to({ angle: 8 }, 1000).to({ angle: 0 }, 1000).loop().start();
        // 點擊開始按鈕的動作
        this.button2.inputEnabled = true;
        this.button2.events.onInputDown.add((obj, pt) => {
            game.state.start('rankState');
        });


    }    

}
